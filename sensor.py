
def enum(**enums):
    return type('Enum', (), enums)

Q=enum(INIT=0,IDLE=1,BUSY=2,AUTO_IDLE=3,AUTO_BUSY=4)
MSG=enum(START=0,STOP=1,MANUAL=2)
class Sensor(obj):
	def __init__(self,T=10,):
		self.state=Q.INIT
		self.msg=None
		self._timePrecision=10
		self.x=0
		self.T=T
	def __wait(self,what,seconds):
		"""delay with conditional interruption if a message is received
		if a message that conforms to what is detected the message is erased (delivered) and returned
		returns None if no message is received after "seconds" sec
		example:
		message= __wait([MSG.stop,MSG.start],12)
		"""
		s=seconds
		while s>self._timePrecision:
			if (self.msg in what):
				msg=self.msg
				self.msg=None
				return msg
			time.sleep(self._timePrecision)
		time.sleep(s)
		return None
	def T(self,seconds):
		self.T=seconds
	def setup(self):
		if not self.state==Q.INIT : return
		self.state=Q.IDLE
		pass
		return
	def fetch(self):
		self.auto=threading.Thread(target=self.__fetch)
		self.auto.daemon=True
		self.auto.start()
	def __fetch(self):
		self.x=0
		return
	def auto(self):
		self.auto=threading.Thread(target=self.__auto)
		self.auto.daemon=True
		self.auto.start()
	def __auto(self):
		while True:
			if not self.__wait([MSG.MANUAL],self.T) ==None :
				return
			self.__fetch()
	def manual(self):
		if self.state not in [Q.AUTO_IDLE,Q.AUTO_BUSY,Q.BUSY]:
			return
		self.msg=MSG.MANUAL
		return

class AnalogSensor(Sensor):
	def __init__(self,arduino, pin):
		self.state=Q.INIT
		self.msg=None
		self._timePrecision=10
		self.T=T
		self.pin=pin
		self.arduino=arduino
	def setup(self):
		if not self.state==Q.INIT : return
		self.state=Q.IDLE
		return
	def __fetch(self):
		self.x=self.arduino.analogRead(self.pin)

	
