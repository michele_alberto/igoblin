#!/usr/bin/python
# -*- coding: utf-8 -*-

import xively
import threading
import time
import datetime
import random


def enum(**enums):
    return type('Enum', (), enums)


Q = enum(INIT=0, CONNECTED=1, CONNECTING=2, DISCONNECTED=3)


class connection(object):

    def __init__(
        self,
        xivelyKey=None,
        feedId=None,
        refreshTime=30,
        retryTime=60,
        ):
        self.state = Q.INIT
        self.API_KEY = xivelyKey \
            or '38XTyTJJrH8z8tIpcdtzc7GYRTbiQcLVxX4QF3N6UljxXnyC'
        self.FEED_ID = feedId or 53795518
        self.debug = False
        self.refreshTime = refreshTime
        self.retryTime = retryTime
        self.uploadQueue = []
        self.connectionService()
        self.queueLimit=500
    def connectLoop(self):
        #print 'init connectLoop'
        while True:
            try:
                if self.state == Q.CONNECTED:
                    time.sleep(self.refreshTime)
                    continue
                if self.state == Q.CONNECTING:
                    print 'connection state is invalid'
                #print 'connectLoop'
                self.state = Q.CONNECTING
                #print 'try connection'
                self.__api = xively.XivelyAPIClient(self.API_KEY)
                #print 'has api'
                self.__feed = self.__api.feeds.get(self.FEED_ID)
                #print 'has feed'
                self.state = Q.CONNECTED
                self.emptyQueue()
                #print 'queue empty'
                time.sleep(self.retryTime)
            except:
                print 'not connecting'
                self.state = Q.DISCONNECTED
                time.sleep(self.retryTime)

    def connectionService(self):
        tm = threading.Timer(self.retryTime * 10,
                             self.connectionService)
        try:
            if not self.connectionThread.isAlive():
                raise
            return
        except:
            self.connectionThread = \
                threading.Thread(target=self.connectLoop)
            self.connectionThread.daemon = True
            self.connectionThread.start()

    def get_datastream(self, feedName):
        try:
            datastream = self.__feed.datastreams.get(feedName)
            #print 'Found existing datastream'
            return datastream
        except:
            print 'Creating new datastream:{}'.format(feedName)
            datastream = self.__feed.datastreams.create(feedName)
            return datastream

    def uploadState(
        self,
        state,
        ts=None,
        prefix='',
        ):

        self.uploadThread = \
            threading.Thread(target=self.__uploadState_task,
                             args=(state, ts, prefix))
        self.uploadThread.daemon = True
        self.uploadThread.start()

    def emptyQueue(self):
        self.queueThread = \
            threading.Thread(target=self.__emptyQueue_task)
        self.queueThread.daemon = True
        self.queueThread.start()

    def __emptyQueue_task(self):
        if not self.state == Q.CONNECTED:
            #print 'not connected , cannot empty queue'
            return
        while len(self.uploadQueue) > 0:
            x = self.uploadQueue.pop()
            self.uploadState(x['state'], x['at'], x['prefix'])

    def __uploadState_task(
        self,
        state,
        ts=None,
        prefix='',
        ):

        if not self.state == Q.CONNECTED:
            #print 'not connected , using queue'
            self.push({'state': state, 'at': ts
                                    or datetime.datetime.now().isoformat(),
                                    'prefix': prefix})
            return
        for a in state:
            if type(state[a]) == dict:
                self.__uploadState_task(state[a], prefix=a + 'pippero')
                continue
            try:
                dts = self.get_datastream(prefix + a)
                #print '@ {0} : {1}'.format(prefix + a,state[a])
                if ts == None:
                    dts.datapoints.create(value=state[a])
                    dts.current_value = state[a]
                else:
                    dts.datapoints.create(value=state[a], at=ts)
                dts.update()
                #print '@ {0} update sent'.format(prefix + a)
            except:
                #print '__uploadState_task:connection error'
                self.state = Q.DISCONNECTED
                self.push({'state': state, 'at': ts
                        or datetime.datetime.now().isoformat(),
                        'prefix': prefix})
                return
    def push(self,args):
        if self.queueLimit>len(self.uploadQueue):
           self.uploadQueue.append(args)
        else:
            randomIndex=random.randint(0,len(self.uploadQueue)-1)
            self.uploadQueue[randomIndex]=args






