import onOf
from datetime import datetime
import datetime
import threading
import time
class Day(onOf.OnOf):
    def __init__(self, arduino, pin,extState=None,extLogger=None,label='dayControl'):
        onOf.OnOf.__init__(self,arduino,pin=pin,extState=extState,extLogger=extLogger,label=label)
        self.target=18 #hours of activity per day
        self.beginT=20
        self.endT=10
        self._timePrecision=1
        self.Ton=1
        self.Toff=60

    def __checknFix(self):
        for i in range(10):
            try:
                self.arduino.ping()
                return ()
            except:
                time.sleep(3)
                self.arduino.close()
                self.arduino.autoconnect()

    def beginDuty(self,h,duty):
        self.beginT=h
        self.endT=h+float(duty)
        if self.endT>24 : self.endT=self.endT-24
    def time(t):
        self.Toff=t        
    def now(self):
        n=datetime.datetime.now()
        now=float(n.hour-n.minute/60.)
        return now
    def __query(self):
        n=self.now()
        if  self.beginT<self.endT:
            if n>self.endT or n<self.beginT :
                return False
            else:
                return True
        else:
            if n>self.beginT or n<self.endT :
                return True
            else:
                return False
    def __update(self):
        q=self.__query()
        #print 'query {0}'.format(q)
        try:
            if q :
                self.on()
            else:
                self.off()
            return
        except:     
            print 'try to reset arduino'
            self.__checknFix()
            self.__update()
            
    def __wait(self,what,seconds):
        """delay with conditional interruption if a message is received
        if a message that conforms to what is detected the message is erased (delivered) and returned
        returns None if no message is received after "seconds" sec
        example:
        message= __wait([MSG.stop,MSG.start],12)
        """
        s=seconds
        while s>self._timePrecision:
            if (self.msg in what):
                msg=self.msg
                self.msg=None
                return msg
            time.sleep(self._timePrecision)
            s=s-self._timePrecision
        time.sleep(s)
        return None
    def __blink(self):
        if not self.state==onOf.Q.IDLE:
            print 'warning: state not valid for blink() : {0}'.format(self.state)
            print 'blink rejected'
            return
        self.msg=None
        self.state=onOf.Q.BLINK
        while True:
            if self.msg==onOf.MSG.MANUAL:
                self.msg==None
                self.state=onOf.Q.IDLE
                return
            self.__update()
            msg=self.__wait([onOf.MSG.MANUAL,onOf.MSG.START],self.Toff)
            if msg==onOf.MSG.MANUAL:
                self.msg==None
                self.state=onOf.Q.IDLE
                return
        self.state=onOf.Q.IDLE
    def blink(self,Ton=None,Toff=None):
        if not Ton==None:
            self.Ton=Ton
        if not Toff==None:
            self.Toff=Toff
        if (self.Ton==None) or (self.Toff==None):
            print "need to specicify Ton and Toff to blink()"
            return
        self.blinking=threading.Thread(target=self.__blink)
        self.blinking.daemon = True
        self.blinking.start()
