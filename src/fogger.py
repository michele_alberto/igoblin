from py_arduino import OUTPUT
from py_arduino.arduino import PyArduino, HIGH, LOW
import threading
import time
import onOf
class Fogger(onOf.OnOf):
    def __init__(self, arduino, fanPin , nebPin , inPumpPin,outPumpPin ,label='fogger',extState=None,extLogger=None):
        onOf.OnOf.__init__(self,arduino,pin=None,extState=extState,extLogger=extLogger,label=label)
        self.fanPin = fanPin
        self.nebPin = nebPin
        self.inPump= onOf.Pwm(arduino,pin=inPumpPin,extState=extState,extLogger=extLogger,label='inPump')
        self.outPump= onOf.Pwm (arduino,pin=outPumpPin,extState=extState,extLogger=extLogger,label='outPump')
        self.outPump.power(255)
        self.inPump.power(90)
        self.state=onOf.Q.INIT
        self.msg=None
        self._timePrecision=1

    def setup(self):
        if not self.state==onOf.Q.INIT:
            print 'warning: setup() in non INIT state: {0}'.format(self.state)
        self.arduino.pinMode(self.fanPin, OUTPUT)
        self.arduino.pinMode(self.nebPin, OUTPUT)
        self.arduino.digitalWrite(self.fanPin, LOW)
        self.arduino.digitalWrite(self.nebPin, LOW)
        self.inPump.setup()
        self.outPump.setup()
        self.CycleRatio(60* 10,0.6)
        self.state=onOf.Q.IDLE
        return
    def goOn(self):
        self.arduino.digitalWrite(self.fanPin, HIGH)
        self.arduino.digitalWrite(self.nebPin, HIGH)
    def goOff(self):
        self.arduino.digitalWrite(self.fanPin, LOW)
        self.arduino.digitalWrite(self.nebPin, LOW)
    def blink_all(self):
        self.blink()
        time.sleep(1)
        self.outPump.blink()
        time.sleep(1)
        self.inPump.blink()
        
