import os
import datetime
import datetime
import shelve
from contextlib import closing

def ensureDir(f):
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)

def ensureFile(f):
    ensureDir(f)
    if not os.path.isfile(f):
        open(f, 'a').close()
    

class Logger():
    def __init__(self,dataFileName,commandFileName):
        if not os.path.isdir('logs/'):
            os.makedirs('logs')
        self.dataFile="logs/"+dataFileName+".txt"     
        self.dataBin="logs/"+dataFileName+".shelve"
        self.commandFile="logs/"+commandFileName+".txt"
        ensureFile(self.dataFile)
        ensureFile(self.commandFile)
        self.lastKey='__last__'
        self.command('[logger] : begin session')
    def command(self,message):
        with open(self.commandFile, 'a') as f:
            now=datetime.datetime.utcnow()
            string="[{0}] + {1} \n".format(now.isoformat(),message)
            f.write(string)

    def data(self,label,value):
        now=datetime.datetime.utcnow()
        with open(self.dataFile , 'a') as f:
            string="{2}[\"{0}\"] + {1} \n".format(now.isoformat(),value,label)
            f.write(string)
        with closing(shelve.open(self.dataBin)) as s:
            if not s.has_key(label):
                s[label]=[]
            s[label]=s[label]+[(now,value)]
            if not s.has_key(self.lastKey):
                s[self.lastKey]={}
            st=s[self.lastKey]
            st[label]=value
            s[self.lastKey]=st
    def getState(self):
        with closing(shelve.open(self.dataBin)) as s:
            return s[self.lastKey]
    def getHistory(self,label):
        with closing(shelve.open(self.dataBin)) as s:
            return s[label]
    def getLabels(self):
        with closing(shelve.open(self.dataBin)) as s:
            ks=s.keys()
            ks.remove(self.lastKey)
            return ks
        
