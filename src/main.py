import onOf
import fogger
import logface
import state as _state
import connection
import time
import day
try:
    from IPython.core.prompts import PromptManager
    from IPython.frontend.terminal.embed import InteractiveShellEmbed
except ImportError:
    print ""
    print ""
    print "ERROR: Couldn't import ipython. You may need to run somthing like:"
    print " $ pip install ipython"
    print ""
    print ""
    raise


from py_arduino import OUTPUT
from py_arduino.main_utils import BaseMain
from py_arduino.arduino import PyArduino, HIGH, LOW
from py_arduino.main_utils import BaseMain

banner = """

----------------------------------------------------------------------
py-arduino
----------------------------------------------------------------------

Launching IPython shell... Enter 'quit()' to exit.

Available variables:
    - arduino: the PyArduino instance.
    - options, args: parsed argument options.
        -modules OnOf ,Sensor, Log
Example:
    >>> arduino.ping()
    'PING_OK'
eseguito:
    fog= fogger.Fogger(arduino,fanPin=3 , nebPin=9 ,inPumpPin=5, outPumpPin=6)
        fog.times(Ton=6*60,Toff=4*60)
        fog.outPump.times(Ton=3,Toff=60*60*4)
        fog.inPump.times(Ton=6,Toff=60*60*4)
    luce=onOf.OnOf(arduino,4)
"""

DEVICES=[]
def manual():
    global DEVICES
    [device.manual() for device in DEVICES]

class Main(BaseMain):

    def run(self, options, args, arduino):
        PromptManager.in_template = "PyArduino [\\#]> "
        PromptManager.out_template = "PyArduino [\\#]: "
        """ QUI IL CODICE DI AVVIO"""
        """1) connessione e persistenza """
        log=logface.Logger("data","commands")
        xiv=connection.connection()
        """2) stato"""
        state=_state.State(connection=xiv,logger=log)
        """3) periferiche"""
        fog= fogger.Fogger(arduino,fanPin=3 , nebPin=9 ,inPumpPin=5, outPumpPin=6,label='fogger',extState=state,extLogger=log)
        fog.times(Ton=6*60,Toff=4*60)
        fog.outPump.times(Ton=3,Toff=60*60*4)
        fog.inPump.times(Ton=6,Toff=60*60*4)
        fog.setup()
        DEVICES.append(fog)
        DEVICES.append(fog.inPump)
        DEVICES.append(fog.outPump)
        luci=day.Day(arduino,pin=4,label='LED',extState=state,extLogger=log)
        luci.Toff=60
        luci.beginT=20
        luci.endT=12
        luci.setup()
        luci.blink()
        DEVICES.append(luci)
        """4) shell"""
        shell = InteractiveShellEmbed(banner2=banner)
        shell.user_ns = {}
        shell()

if __name__ == '__main__':
    Main().start()
