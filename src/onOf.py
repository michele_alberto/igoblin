#!/usr/bin/python
# -*- coding: utf-8 -*-
from py_arduino import OUTPUT
from py_arduino.arduino import PyArduino, HIGH, LOW
import threading
import time


def enum(**enums):
    return type('Enum', (), enums)


Q = enum(
    INIT=0,
    IDLE=1,
    BUSY=2,
    BLINK=4,
    B_BUSY=5,
    B_IDLE=6,
    )
MSG = enum(START=0, STOP=1, MANUAL=2)


class OnOf(object):

    """classe che usa un pin alternando tra stati digital write HIGH/LOW"""

    def __init__(
        self,
        arduino,
        pin,
        extState=None,
        extLogger=None,
        label=None
        ):
        self.arduino = arduino
        self.pin = pin
        self.state = Q.INIT
        self.msg = None
        self._timePrecision = 1
        self.Ton = 1
        self.Toff = 1
        self.extState = extState
        self.extLogger = extLogger
        self.label = label or 'DIGITAL{}'.format(pin)
        self.debug = False

    def talk(self, s):
        if not self.label == None:
            say = ('[{0}] : '.format(self.label)) + s
        else:
            say = s
        if not self.extLogger == None:
            self.extLogger.command(say)
        print say

    def mumble(self, s):
        if self.debug:
            self.talk(s)

    def __checknFix(self):
        for i in range(10):
            try:
                self.arduino.ping()
                return ()
            except:
                self.arduino.close()
                self.arduino.autoconnect()

    def times(self, Ton, Toff):
        self.talk('times : {} / {} '.format(Ton,Toff))
        self.Ton = Ton
        self.Toff = Toff

    def CycleRatio(self, T, r):
        if r > 1 or r < 0:
            return
        self.times( T * r,T * (1 - r))

    def setup(self):
        if not self.state == Q.INIT:
            self.talk('warning: setup() in non INIT state: {0}'.format(self.state))
        self.arduino.pinMode(self.pin, OUTPUT)
        self.arduino.digitalWrite(self.pin, LOW)
        self.state = Q.IDLE

    def on(self):
        self.talk('ON')
        if not self.extState==None: 
            self.extState[self.label+'XXv']=1
        self.goOn()

    def goOn(self):
        self.arduino.digitalWrite(self.pin, HIGH)

    def off(self):
        self.goOff()
        self.talk('OFF')
        if not self.extState==None: 
            self.extState[self.label+'XXv']=0

    def goOff(self):
        self.arduino.digitalWrite(self.pin, LOW)

    def __wait(self, what, seconds):
        """delay with conditional interruption if a message is received
        if a message that conforms to what is detected the message is erased (delivered) and returned
        returns None if no message is received after "seconds" sec
        example:
        message= __wait([MSG.stop,MSG.start],12)
        """

        s = seconds
        while s > self._timePrecision:
            if self.msg in what:
                msg = self.msg
                self.msg = None
                return msg
            time.sleep(self._timePrecision)
            s = s - self._timePrecision
        time.sleep(s)
        return None

    def __erogation(self, seconds):
        """
        execute an erogation
        seconds: seconds of fan erogation
        """

        prevState = self.state
        if not self.state in [Q.IDLE, Q.BLINK, Q.B_IDLE]:
            self.mumble('warning: state not valid for erogation() : {0}'.format(self.state))
            self.talk('erogation rejected')
            return
        if self.state == Q.IDLE:
            self.state = Q.BUSY
        else:
            self.state = Q.B_BUSY
        self.mumble('erogation for {0} seconds'.format(seconds))
        try:
            self.on()
        except:
            self.talk('try to reset arduino')
            self.__checknFix()
            self.state = prevState
            self.__erogation(seconds)
            return
        msg = self.__wait([MSG.STOP, MSG.MANUAL], seconds)
        offEffective = False
        while not offEffective:
            try:
                self.off()
                offEffective = True
            except:
                self.talk('try to reset arduino')
                self.__checknFix()
        if self.state == Q.BUSY:
            self.state = Q.IDLE
            return
        if self.state == Q.B_BUSY and msg in [MSG.STOP, None]:
            self.state = Q.B_IDLE
            return
        if self.state == Q.B_BUSY and msg == MSG.MANUAL:
            self.msg = MSG.MANUAL
            self.state = Q.B_IDLE
            return
        self.talk('if you read this line then code reached an unexpected state'
                  )
        return

    def erogation(self, seconds):
        if not self.state in [Q.IDLE, Q.BLINK]:
            self.mumble('warning: state not valid for erogation() : {0}'.format(self.state))
            self.talk('erogation rejected')
            return
        self.erogating = threading.Thread(target=self.__erogation,
                args=(seconds, ))
        self.erogating.daemon = True
        self.erogating.start()
        return self.erogating

    def __blink(self):
        if not self.state == Q.IDLE:
            self.mumble('warning: state not valid for blink() : {0}'.format(self.state))
            self.talk('blink rejected')
            return
        self.msg = None
        self.state = Q.BLINK
        while True:
            if self.msg == MSG.MANUAL:
                self.msg == None
                self.state = Q.IDLE
                return
            self.__erogation(self.Ton)
            msg = self.__wait([MSG.MANUAL, MSG.START], self.Toff)
            if msg == MSG.MANUAL:
                self.msg == None
                self.state = Q.IDLE
                return
        self.state = Q.IDLE
        return

    def blink(self, Ton=None, Toff=None):
        if not Ton == None:
            self.Ton = Ton
        if not Toff == None:
            self.Toff = Toff
        if self.Ton == None or self.Toff == None:
            self.talk('need to specicify Ton and Toff to blink()')
            return
        self.talk('BLINK')
        self.blinking = threading.Thread(target=self.__blink)
        self.blinking.daemon = True
        self.blinking.start()
        return self.blinking

    def start(self):
        if self.state == Q.B_IDLE:
            self.msg = MSG.START
        self.talk('START')

    def stop(self):
        if self.state in [Q.B_BUSY, Q.BUSY]:
            self.msg = MSG.STOP
        self.talk('STOP')

    def manual(self):
        self.talk('MANUAL')
        while not self.state == Q.IDLE:
            self.msg = MSG.MANUAL
            time.sleep(2)
        self.talk('now in manual mode: state is IDLE')


class Pwm(OnOf):

    def __init__(
        self,
        arduino,
        pin,
        extState=None,
        extLogger=None,
        label=None,
        power=127
        ):
        OnOf.__init__(self,arduino,pin=pin,extState=extState,extLogger=extLogger,label=label)
        self.state = Q.INIT
        self.msg = None
        self._timePrecision = 1
        self.Ton = 1
        self.Toff = 1
        self._power = power

    def goOn(self):
        self.arduino.analogWrite(self.pin, self._power)

    def goOff(self):
        self.arduino.digitalWrite(self.pin, LOW)

    def power(self,p):
        self.talk('power:{0}'.format(p))
        self._power = p


