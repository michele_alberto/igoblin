import datetime
import threading
class State(dict):
    def __init__(self,connection,logger):
        self.xiv=connection
        self.log=logger
        self.lastUpdate={}
        self.updateTime=30
    def __setitem__(self, key, value):
        self.log.data(key,value)
        self.lastUpdate[key]=datetime.datetime.now()
        return dict.__setitem__(self, key, value)
    def freshness(self,targets=None):
        now=datetime.datetime.now()
        filtered={x:(now-self.lastUpdate[x]) for x in self.lastUpdate if (targets==None) or (x==targets) or (x in targets)}
        return filtered
    def update(self):
        self.xiv.uploadState(self)
    def updateLoop(self):
        self.uploadThread=threading.Timer(self.updateTime,self.updateLoop)
        self.uploadThread.daemon=True
        self.uploadThread.start()
        self.update()
        
        
