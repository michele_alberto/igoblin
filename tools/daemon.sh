#!/bin/bash

LOG_FILE="DaemonLog.txt"
function logit {
    echo "[${USER}][`date`] - ${*}" >> ${LOG_FILE}
    echo "[${USER}][`date`] - ${*}"
}
export PYTHONPATH='py-arduino/'
while true;
do 
logit 'arduino listener started'
python2.7 src/main.py /dev/ttyACM* 
logit 'arduino listener stopped' 
done
