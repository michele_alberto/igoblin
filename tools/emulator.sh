#!/bin/bash

LOG_FILE="DaemonLog.txt"
function logit {
    echo "[${USER}][`date`] - ${*}" >> ${LOG_FILE}
    echo "[${USER}][`date`] - ${*}"
}

export PYTHONPATH='py-arduino/'
python src/main.py /dev/ARDUINO_EMULATOR 

